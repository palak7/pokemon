import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
	API_URL  =  'https://pokeapi.co/api/v2/';
	constructor(private http: HttpClient) {}
	getPokemonList(){
		return this.http.get(`${this.API_URL}pokedex/`);
	}
	getPokemonDetails(pId){
		return this.http.get(`${this.API_URL}pokedex/${pId}/`);
	}
}
