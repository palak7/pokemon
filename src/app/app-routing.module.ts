import { NgModule } from '@angular/core';
import  {RouterModule, Routes} from '@angular/router';
//pokemon compoenent
import {ListPokemonComponent} from './pokemon/list-pokemon/list-pokemon.component';
import {DetailPokemonComponent} from './pokemon/detail-pokemon/detail-pokemon.component';

const appRoutes: Routes = [
	{ 
		path:'pokemon/list', 
		component:ListPokemonComponent
	},
	{ 
		path:'pokemon/details/:id', 
		component:DetailPokemonComponent
	},
	{ 
		path:'', 
		redirectTo:'/pokemon/list', 
		pathMatch:'full' 
	},
];

@NgModule({
  imports: [
  	RouterModule.forRoot(appRoutes),
  ],
   exports: [RouterModule]
})
export class AppRoutingModule { }
