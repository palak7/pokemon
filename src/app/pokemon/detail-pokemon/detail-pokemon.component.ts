import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../../api.service';
import { ActivatedRoute } from "@angular/router";
import { ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

			
@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.css']
})
export class DetailPokemonComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	length = 100;
	pageSize = 10;
	pokemon$: Object;
	pokemonDetails$: Array<object> = [];
	public details = new MatTableDataSource();
  displayedColumns = [ 'entry_number','name'];	
  	constructor(private route: ActivatedRoute,private  apiService:  ApiService) { 
    	this.route.params.subscribe( params => this.pokemon$ = params.id );
  	}
		ngAfterViewInit() {
			// add ngAfterViewInit hook
				 this.details.paginator = this.paginator;
		}
  ngOnInit() {
  	 
  	this.apiService.getPokemonDetails(this.pokemon$).subscribe(
					data => {
						this.pokemonDetails$ = data,
						this.details = data.pokemon_entries
					} 
					
					
	    );
  	
	}
	

}
