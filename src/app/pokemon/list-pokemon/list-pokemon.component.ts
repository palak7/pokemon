import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../../api.service';
import { Router } from '@angular/router';
import { ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

			 
@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.css']
})
export class ListPokemonComponent implements OnInit {
@ViewChild(MatPaginator) paginator: MatPaginator;
pokemons$: Array<object> = [];
length = 100;
pageSize = 10;
displayedColumns = [ 'name'];
public list = new MatTableDataSource();
  constructor(private  apiService:  ApiService,private router: Router) { }
	
	ngAfterViewInit() {
			 this.list.paginator = this.paginator;
	}
  ngOnInit() {
  	  this.getPokemon();
  }
	public  getPokemon(){
	    this.apiService.getPokemonList().subscribe(
					data => this.list.data = data.results
	   	);
	}
	public setLink(url) {
		var url = url.split( '/' );
		var pokemonId = url[ url.length - 2 ];
		this.router.navigate(['pokemon/details/',pokemonId]);
	}
}
